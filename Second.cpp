#include <iostream>
#include <ctime>
using namespace std;
void func(int arr[], int& count, int size, int b) {
  for (int i = 0; i < size; i++)
  {
    arr[i] = rand() % 100;
    cout << arr[i] << "\t";
    if (arr[i] < b) { count++; };
  }
  }
int main() {
  srand(time(NULL));
  int size, b, count = 0;
  cout << "enter the number of items in the sequence" << endl << "n = ";
  cin >> size;
  cout << "enter a number b = ";
  cin >> b;
  int* arr = new int[size];
  func(arr, count, size, b);
  cout << endl<<"count = " << count;
  delete[] arr;
}
