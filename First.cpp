#include <iostream>
#include <ctime>
using namespace std;
  
void swap(int& a, int& b) {
  int c;
  c = a;
  a = b;
  b = c;
}
 
int main() {
  srand(time(NULL));
  const int size = 10;
  int arr[size]{};
  cout << "old array: ";
  for (int i = 0; i < size; i++)
  {
    arr[i] = rand() % 100;
    cout << arr[i] << "\t";
  }
  cout << endl << "new array: ";
  for  (int i = 0, j = size-1;i<=j-1; i++, j--)
  {
    swap(arr[i], arr[j]);
  }
  for (int i = 0; i < size; i++)
  {
    cout << arr[i] << "\t";
  }
    return 0;
}
